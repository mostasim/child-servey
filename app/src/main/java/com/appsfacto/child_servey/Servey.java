package com.appsfacto.child_servey;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Servey extends AppCompatActivity {
    EditText etName,etAge;
    Button btName;
    String name,age;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_servey);
        etName= (EditText) findViewById(R.id.etName);
        etAge= (EditText) findViewById(R.id.etAge);
        btName= (Button) findViewById(R.id.btName);

        btName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name=etName.getText().toString();
                age=etAge.getText().toString();
                Intent intent=new Intent(getApplicationContext(),Question.class);
                intent.putExtra("Name",name);
                intent.putExtra("Age",age);
               // Toast.makeText(getApplicationContext(),"N:"+name,Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }


}
