package com.appsfacto.child_servey;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ReslutActivity extends AppCompatActivity {
    String name,age;
    TextView tvName,tvAge;
    ListView lvResult;
    int physicalDisability=0,intellectualDisability=1,visualImpairment=2,hearingImpairment=3,epilepsy=4,
            learningDisability=5,communicationDisability=6,downSyndrome=7;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reslut);

        //initialization of TextView & ListView
        tvName= (TextView) findViewById(R.id.tvName);
        tvAge= (TextView) findViewById(R.id.tvAge);
        lvResult= (ListView) findViewById(R.id.lvResult);
        //Get Bundle intent
        Bundle bundle=getIntent().getExtras();
        int res[]=bundle.getIntArray("Result");
        name=bundle.getString("Name");
        age=bundle.getString("Age");

        //Set TextView
        tvName.setText("Name: "+name);
        tvAge.setText("Age: "+age);

        //Set List View
        List<String> arrayList=new ArrayList<String>();
        arrayList.add("Physical Disability: "+String.valueOf(res[0]));
        arrayList.add("Intellectual Disability: "+String.valueOf(res[1]));
        arrayList.add("Visual Impairment: "+String.valueOf(res[2]));
        arrayList.add("Hearing Impairment: "+String.valueOf(res[3]));
        arrayList.add("Epilepsy: "+String.valueOf(res[4]));
        arrayList.add("Learning Disability: "+String.valueOf(res[5]));
        arrayList.add("Communication Disability: "+String.valueOf(res[6]));
        arrayList.add("Down Syndrome: "+String.valueOf(res[7]));
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arrayList);
        lvResult.setAdapter(adapter);


    }
    public void showMessage(String msg)
    {
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
    }
}
