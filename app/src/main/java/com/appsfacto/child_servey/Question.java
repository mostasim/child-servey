package com.appsfacto.child_servey;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class Question extends AppCompatActivity {

    int [] statsServey= new int[8];
    int physicalDisability=0,intellectualDisability=1,visualImpairment=2,hearingImpairment=3,epilepsy=4,
    learningDisability=5,communicationDisability=6,downSyndrome=7;
    String[] question,questionNo;
    RadioButton rbYes,rbNo;
    String name="",q,answer,yes="Yes",no="No";
    int qNo,index=0,age=0;
    TextView tvQuestion,tvQNo;
    Button btNext;
    boolean isSelectRb=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);
        //Set stastservey 0
        Arrays.fill(statsServey,0);

        //Set TextView & Button
        tvQuestion= (TextView) findViewById(R.id.tvQuestion);
        tvQNo= (TextView) findViewById(R.id.tvQuesNo);
        btNext= (Button) findViewById(R.id.btNext);

        //Get Name & Age
        Bundle bundle=getIntent().getExtras();
        name=bundle.getString("Name");
        age=Integer.valueOf(bundle.getString("Age"));

        //Show Message
        Toast.makeText(getApplicationContext(),"Name: "+name,Toast.LENGTH_SHORT).show();

        //Get Question Array
        question=getResources().getStringArray(R.array.question);
        questionNo=getResources().getStringArray(R.array.questionNo);
        //Set Radio Buttons
        rbYes= (RadioButton) findViewById(R.id.rbYes);
        rbNo= (RadioButton) findViewById(R.id.rbNo);
        q=question[index];
        q = q.replace("[NAME]", name); //Replace [NAME] in evry Question with child name.
        qNo=1;
        q=q.toLowerCase();
        tvQNo.setText("Q: "+String.valueOf(qNo));
        tvQuestion.setText(q);



        rbYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rbYes.setChecked(true);
                rbNo.setChecked(false);
                answer=yes;
                isSelectRb=true;

            }
        });

        rbNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rbNo.setChecked(true);
                rbYes.setChecked(false);
                answer=no;
                isSelectRb=true;
            }
        });

        btNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(btNext.getText().equals("Next"))
                {
                    if (isSelectRb) {

                       if ((index==0 && rbYes.isChecked()))
                        {
                            nextQuestion();
                        }else if (index==8 && rbNo.isChecked())
                       {
                           nextQuestion();
                       }else
                        {
                            if (index==0)//For skip Sub Question 1.1
                                index=1;
                            if (index==8) // For Skip Sub Question 8.1
                                index=9;
                            if ((index==8 || index==9)&&age>=3 && age<=9)//For 9A
                                index=9;
                            else if((index==8 || index==9)&&age<=2) //For 9B
                                index=10;

                            nextQuestion();
                        }
                       // nextQuestion();
                    }
                    else
                        showMessage("Select any of the button Yes/No ");

                }else if (btNext.getText().equals("Finish"))
                {
                    //Do Calculation Result
                    if (isSelectRb) {
                        showMessage("Thanks you");
                        Intent intent=new Intent(getApplicationContext(),ReslutActivity.class);
                        intent.putExtra("Result",statsServey);
                        intent.putExtra("Name",name);
                        intent.putExtra("Age",String.valueOf(age));
                        startActivity(intent);
                        String res="";
                        for (int i=0;i<8;i++)
                        {
                            res+=String.valueOf(statsServey[i])+"\n";
                        }
                       // showMessage(res);


                    }
                    else
                        showMessage("Select any of the button Yes/No ");

                }

            }
        });



    }
    public  void nextQuestion()
    {
        calculateServey();
        int length=question.length;
        index++;
        if (index==(length-1)) {
            btNext.setText("Finish");//Set Finish Button in the End
        }
        q=question[index];
        q = q.replace("[NAME]", name);
        q=q.toLowerCase();
        tvQNo.setText("Q: "+questionNo[index]);
        tvQuestion.setText(q);
        if (index==10)
            index++; //For skip 9B by age
        rbYes.setChecked(false);
        rbNo.setChecked(false);
        isSelectRb=false;
    }
    public void showMessage(String msg)
    {
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
    }
    public void calculateServey()
    {
        switch (index)
        {
            case 1:
                if (answer.equals(yes))
                    statsServey[physicalDisability]++;
                else
                    statsServey[intellectualDisability]++;
                break;
            case 2:
                if (answer.equals(yes))
                    statsServey[visualImpairment]++;
                break;
            case 3:
                if (answer.equals(yes))
                    statsServey[hearingImpairment]++;
                break;
            case 4:
                if (answer.equals(no))
                {
                    statsServey[intellectualDisability]++;
                    statsServey[hearingImpairment]++;
                }
                break;
            case 5:
                if (answer.equals(yes))
                    statsServey[physicalDisability]++;
                break;
            case 6:
                if (answer.equals(yes))
                    statsServey[epilepsy]++;
                break;
            case 7:
                if (answer.equals(yes))
                {
                    statsServey[intellectualDisability]++;
                    statsServey[learningDisability]++;
                }
                break;
            case 8:
                break;
            case 9:
                if (answer.equals(yes))
                    statsServey[intellectualDisability]++;
                if (answer.equals(no))
                    statsServey[communicationDisability]++;
                break;
            case 10:
                if (answer.equals(yes))
                {
                    statsServey[intellectualDisability]++;
                    statsServey[communicationDisability]++;
                }
                break;
            case 11:
                if (answer.equals(no))
                {
                    statsServey[intellectualDisability]++;
                    statsServey[communicationDisability]++;
                }
                break;
            case 12:
                if (answer.equals(yes))
                    statsServey[intellectualDisability]++;
                break;
            case 13:
                if (answer.equals(yes))
                    statsServey[intellectualDisability]++;
                break;
            case 14:
                if (answer.equals(yes))
                    statsServey[downSyndrome]++;
                break;
            default:
                break;
        }
    }
}
